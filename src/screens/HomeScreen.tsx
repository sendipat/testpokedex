/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useRef, useMemo, useCallback, useEffect } from 'react';
import {
  Pressable,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  ImageBackground,
} from 'react-native';
import { IcLogo, IcBar, IcBackground, IcClose } from './../assets/index';
import image from './../assets/Image'
import { IPoke, IPokeResponse, MenuPokemon } from './../type/type';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import colors from './../assets/colors/Colors'
import HeaderModal from '../components/HeaderModal';
import CardDetail from '../components/CardDetail';
import CardList from '../components/CardList';
import { connect } from 'react-redux';
import {
  BottomSheetBackdrop,
  BottomSheetModal,
  BottomSheetModalProvider,
} from '@gorhom/bottom-sheet';
import {
  getPokemonList,
  getPokemonType,
  getPokemonListPaging
} from './../redux/screenActions/Home/actions';
import { code_color } from './../utils/ArrayColor';

import i18n from './../i18n';
import i18next from 'i18next';
import Header from '../components/Header';

export interface IMenu {
  id: string;
  name: string;
}
const MENU = [{
  id: 'home',
  name: 'Home'
}, {
  id: 'pockemonType',
  name: 'Pockemon Type'
}]

export interface ICard {
  id: number;
  name: string;
  color: string;
}
const type = [{
  id: 0,
  name: 'Type 1',
  color: '#E66D00',
}, {
  id: 1,
  name: 'Type 2',
  color: '#DE2C2C',
},
{
  id: 2,
  name: 'Type 2',
  color: '#01B956',
},
{
  id: 3,
  name: 'Type 2',
  color: '#E34C88',
},
{
  id: 4,
  name: 'Type 2',
  color: '#8E44AD',
}
]



const HomeScreen = props => {
  const [screen, setScreen] = useState<string>(props.route.name)
  const bottomSheetRef = useRef<BottomSheetModal>(null);
  const [isEnabled, setIsEnabled] = useState<boolean>(false);
  const [isBottom, setIsBottom] = useState<boolean>(false);
  const [menuList, setMenuList] = useState<IMenu[]>(MENU);
  const [list, setlist] = useState<IPoke[]>([]);
  const [typeList, setTypeList] = useState<ICard[]>(type);
  const [paging, setPaging] = useState<boolean>(false);
  const [detailCard, setDetailCard] = useState<IPoke[]>([]);
  const isDarkMode = useColorScheme() === 'dark';
  const [dataSourceCords, setDataSourceCords] = useState([] as number[]);
  const [scrollToIndex, setScrollToIndex] = useState(0);
  const [ref, setRef] = useState<ScrollView>();
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : colors.white,
  };
  useEffect(() => {
    props.getPokemonList()
    props.getPokemonType()
  }, [])

  useEffect(() => {
    if (paging === false) {
      setlist(props.pokemonList)
    } else {
      setlist(list.concat(props.pokemonList))
    }
  }, [props.pokemonList, paging])

  const pressType = (data: string, url: string) => {
    props.navigation.navigate('Type Screen', { id: data, url })
  }
  const pressLoad = (url: string) => {
    setPaging(true)
    props.getPokemonListPaging(url)
  }

  const scrollHandler = (key: number) => {
    if (dataSourceCords.length > scrollToIndex) {
      ref?.scrollTo({
        x: 0,
        y: dataSourceCords[key], //we get the offset value from array based on key
        animated: true,
      });
    }
  };

    const snapPoints = useMemo(() => ['60%', '95%'], []);

    const handlePresentModalPress = useCallback(() => {
      bottomSheetRef.current?.present();
    }, []);
    const handleSheetChanges = useCallback((index: number) => {
      console.log('handleSheetChanges', index);
    }, []);

  const card = (item: any, i: number) => {
    return (
      <CardList item={item} onPressDetail={() => {handlePresentModalPress(), setDetailCard(item)}} onPressType={(value, url) => pressType(value, url)}/>
    )
  }

  const renderList = () => (
    <ScrollView >
      <ImageBackground source={image.Background} resizeMode="cover" style={{
        flex: 1,
      }}>
        <Text style={styles.titleContent}>PokèDex</Text>
        <Text style={styles.titleSubcontent}>{i18n.t('total')}{' '}
          {props?.pokemonData?.count} Pokemon</Text>
        {list?.length > 0 && list.map((item: IPoke, i: number) => (
          card(item, i)
        ))}
        {props.pokemonData?.next != null ? <Pressable onPress={() => pressLoad(props.pokemonData?.next)} style={{ alignItems: 'center', padding: 10, backgroundColor: '#96D9D6' }}>
          <Text style={styles.titleSubcontent}>{i18n.t('loadMore')} ...</Text>
        </Pressable> : null}
      </ImageBackground>
    </ScrollView>
  )

  const renderContent = () => (
    <ScrollView ref={ref => {
      setRef(ref as any); //set the ref
    }} >
      <View
        key={1} //keys will be needed for function
        onLayout={event => {
          const layout = event.nativeEvent.layout;
          dataSourceCords[1] = layout.y; // we store this offset values in an array
        }} >
        <View style={{ marginHorizontal: 20, marginVertical: 50 }}>
          <View style={{ alignItems: 'center', marginBottom: 20 }}>
            <Image source={image.Icon} style={styles.imageMain} resizeMode='contain' />
          </View>
          <Text style={styles.titleMain}>{i18n.t('title')}</Text>
          <Text style={styles.subMain}>{i18n.t('subtitle')}</Text>
          <Pressable onPress={() => scrollHandler(2)} style={styles.cardCheck}>
            <Text style={styles.titleCardCheck}>{i18n.t('check')}</Text>
          </Pressable>
        </View>
      </View>

      <View
        key={2} //keys will be needed for function 
        onLayout={event => {
          const layout = event.nativeEvent.layout;
          dataSourceCords[2] = layout.y; // we store this offset values in an          array
        }}>
        {renderList()}
      </View>
    </ScrollView >
  )

  const renderBackdrop = useCallback(
    props => (
      <BottomSheetBackdrop
        {...props}
        disappearsOnIndex={-1}
        appearsOnIndex={1}
        opacity={1}
        pressBehavior="close"
      />
    ),
    []
  );

  return (
    <SafeAreaView style={[backgroundStyle, { flex: 1 }]} >
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <HeaderModal visible={isEnabled} onClose={(value: boolean) => setIsEnabled(value)} menuList={menuList} pokemonType={props.pokemonType} pressMenu={() => { setIsEnabled(false) }} pressType={(value: string, url: url) => { pressType(value, url), setIsEnabled(false) }} />
      <Header isEnabled={isEnabled} onPressDetail={(value: boolean) => setIsEnabled(value)} backgroundStyle={backgroundStyle} />
      {renderContent()}
      <BottomSheetModalProvider>
        <BottomSheetModal
          ref={bottomSheetRef}
          index={0}
          backdropComponent={renderBackdrop}
          snapPoints={snapPoints}
          onChange={handleSheetChanges}
        >
          < CardDetail detailCard={detailCard} onPressType={(value: string, url: string) => { bottomSheetRef.current?.dismiss(), pressType(value, url) }} onPressDetail={() => { bottomSheetRef.current?.dismiss(), props.navigation.navigate('Detail Screen', { data: detailCard }) }} />
        </BottomSheetModal>
      </BottomSheetModalProvider>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  imageCard: { height: 100, width: 100 },
  titleName: { color: '#42494D', fontSize: 36, lineHeight: 48, fontWeight: '700', marginBottom: 10, textAlign: 'left' },
  subTitle: { color: '#42494D', fontSize: 16, lineHeight: 30, fontWeight: '700', },
  rowCenter: { flexDirection: 'row', alignItems: 'center' },
  subAbilities: { color: '#42494D', fontSize: 14, lineHeight: 18, fontWeight: '400', textAlign: 'center', marginVertical: 5 },
  titleSmallCard: { color: colors.white, fontSize: 14, lineHeight: 18, fontWeight: '700', textAlign: 'center' },
  cardType: { paddingVertical: 10, paddingHorizontal: 20, margin: 5, borderRadius: 20 },
  cardButton: { backgroundColor: '#E6AB09', alignItems: 'center', width: 120, padding: 10, borderRadius: 15 },
  imageMain: { width: 300, height: 300 },
  titleMain: { color: '#42494D', fontSize: 38, lineHeight: 40, fontWeight: '700', marginBottom: 10 },
  subMain: { color: '#7B8082', fontSize: 22, lineHeight: 35, fontWeight: '400', marginBottom: 10 },
  cardCheck: { backgroundColor: '#E6AB09', width: 240, borderRadius: 10, padding: 5, alignItems: 'center' },
  titleCardCheck: { color: colors.white, fontSize: 20, lineHeight: 30, fontWeight: '700' },
  titleContent: { color: '#42494D', fontSize: 38, lineHeight: 40, fontWeight: '700', marginBottom: 10, textAlign: 'center', marginTop: 20 },
  titleSubcontent: { color: '#42494D', fontSize: 20, lineHeight: 30, fontWeight: '400', marginBottom: 10, textAlign: 'center', marginHorizontal: 20 },
  cardList: { backgroundColor: colors.white, marginHorizontal: 20, marginVertical: 10, padding: 20, borderRadius: 15 },
  titleNumber: { color: '#B3B6B8', fontSize: 14, lineHeight: 16, fontWeight: '700', marginBottom: 10 },
  cardName: { color: '#42494D', fontSize: 24, lineHeight: 30, fontWeight: '700', marginBottom: 10 }
});
const mapStateToProps = state => {
  const { pokemonList, pokemonData, pokemonType } = state.home;
  return {
    pokemonList, pokemonData, pokemonType
  };
};

export default connect(mapStateToProps, { getPokemonList, getPokemonType, getPokemonListPaging })(HomeScreen);

