/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useRef, useMemo, useCallback, useEffect } from 'react';
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    ImageBackground,
} from 'react-native';
import { IcLogo, IcBar, IcBackground, IcClose } from './../assets/index';
import image from './../assets/Image'
import { IPoke, IPokeResponse, MenuPokemon } from './../type/type';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import colors from './../assets/colors/Colors'
import HeaderModal from '../components/HeaderModal';
import CardDetail from '../components/CardDetail';
import CardList from '../components/CardList';
import { connect } from 'react-redux';
import {
    BottomSheetBackdrop,
    BottomSheetModal,
    BottomSheetModalProvider,
} from '@gorhom/bottom-sheet';
import {
    getPokemonList,
    getPokemonType,
    getPokemonListPaging
} from './../redux/screenActions/Home/actions';
import { code_color } from './../utils/ArrayColor';

import i18n from './../i18n';
import i18next from 'i18next';
import Header from '../components/Header';
import axios from 'axios';
import { FlatList } from 'react-native-gesture-handler';

export interface IMenu {
    id: string;
    name: string;
}
const MENU = [{
    id: 'home',
    name: 'Home'
}, {
    id: 'pockemonType',
    name: 'Pockemon Type'
}]

export interface ICard {
    id: number;
    name: string;
    color: string;
}
const type = [{
    id: 0,
    name: 'Type 1',
    color: '#E66D00',
}, {
    id: 1,
    name: 'Type 2',
    color: '#DE2C2C',
},
{
    id: 2,
    name: 'Type 2',
    color: '#01B956',
},
{
    id: 3,
    name: 'Type 2',
    color: '#E34C88',
},
{
    id: 4,
    name: 'Type 2',
    color: '#8E44AD',
}
]



const DiscoverScreen = props => {
    const [isEnabled, setIsEnabled] = useState<boolean>(false);
    const [detailCard, setDetailCard] = useState<[]>([]);
    const isDarkMode = useColorScheme() === 'dark';
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : colors.white,
    };
    useEffect(() => {
        const pullData = async () => {
            fetchList().then((result) => {
                setDetailCard(result?.data?.dataseries)
            });
        }
        pullData()

    }, [])

    const fetchList = async () => {
        try {
            return await axios.get('https://www.7timer.info/bin/api.pl?lon=113.17&lat=23.09&product=civil&output=json')
        } catch (err) { console.log('error fetching data from Source', err) }
    }

    return (
        <SafeAreaView style={[backgroundStyle, { flex: 0 }]} >
            <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
            <FlatList
                contentContainerStyle={{ flex: 0 }}
                scrollEventThrottle={64}
                data={detailCard}
                initialNumToRender={100} // Reduce initial render amount
                maxToRenderPerBatch={50} // Reduce number in each render batch
                windowSize={3}
                disableVirtualization
                removeClippedSubviews={false}
                showsVerticalScrollIndicator={false}
                renderItem={({ item }) => (
                    <View style={{ backgroundColor: code_color.primary, margin: 10, padding: 10, borderRadius: 15, alignItems: 'center' }}>
                        <Text style={styles.subTitle}>Cuaca Hari ini : {item?.weather}</Text>
                        <Text style={styles.titleName}>Kerapatan Awan :{item?.cloudcover}</Text>
                        <Text  style={styles.titleName}>Titik Waktu : {item?.timepoint}</Text>
                        <Text  style={styles.titleName}>{item?.rh2m}</Text>

                    </View>
                )}
                keyExtractor={(item, i) => i.toString()}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    imageCard: { height: 100, width: 100 },
    titleName: { color: '#42494D', fontSize: 14,fontWeight: '700', marginBottom: 10, textAlign: 'left' },
    subTitle: { color: code_color.electric, fontSize: 16, lineHeight: 30, fontWeight: '700', },
});
const mapStateToProps = state => {
    const { pokemonList, pokemonData, pokemonType } = state.home;
    return {
        pokemonList, pokemonData, pokemonType
    };
};

export default connect(mapStateToProps, { getPokemonList, getPokemonType, getPokemonListPaging })(DiscoverScreen);

