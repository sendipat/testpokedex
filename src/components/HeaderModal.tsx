
import * as React from 'react';
import { View, Image, Modal, Pressable, SafeAreaView, Dimensions, Text } from 'react-native';
import { IcLogo, IcBar, IcBackground, IcClose } from './../assets/index';
import { code_color } from './../utils/ArrayColor';
import colors from './../assets/colors/Colors'
import i18n from './../i18n';

interface IMenu {
    visible: boolean;
    onClose?: () => void;
    pressMenu?: () => void;
    pressType?: () => void;
    menuList?: any;
    pokemonType?: React.ReactNode;
}

const HeaderModal = ({ visible, onClose, menuList, pokemonType, pressMenu, pressType }: IMenu) => {
    return (
        <Modal
            animationType="fade"
            visible={visible}
            style={{
                top: 0,
                width: Dimensions.get('screen').width,
                alignItems: 'center',
                justifyContent: 'center',
                position: 'absolute',
            }}>
            <SafeAreaView>
                <View style={{ flexDirection: 'row', marginHorizontal: 20, marginVertical: 20 }}>
                    <View style={{ flex: 1 }}>
                        <IcLogo />
                    </View>
                    <Pressable onPress={() => onClose(!visible)} style={{ flex: 1, alignItems: 'flex-end' }}>
                        <IcClose />
                    </Pressable>
                </View>
                <View style={{ marginHorizontal: 20, marginVertical: 40, }}>
                    {menuList.map((item, index) => (
                        <Pressable key={index} onPress={() => pressMenu()}>
                            <Text style={{ color: item.name === 'Home' ? '#E6AB09' : '#42494D', fontSize: 16, lineHeight: 24, fontWeight: item.name === 'Home' ? '700' : null, marginBottom: 10 }} key={index}>{i18n.t(item.id)}</Text>
                            {menuList.length === index + 1 ? null : <View style={{ borderWidth: 0.5, borderColor: 'gray', marginBottom: 10 }} />}
                        </Pressable>

                    ))}
                    <View style={{ flex: 0, flexDirection: 'row', flexWrap: 'wrap' }}>
                        {pokemonType.length > 0 && pokemonType.map((value, i) => (
                            <Pressable onPress={() => pressType(value.name, value.url)} style={{ paddingVertical: 10, paddingHorizontal: 20, backgroundColor: code_color[`${value?.name}`], margin: 5, borderRadius: 20 }} >
                                <Text style={{ color: colors.white, fontSize: 14, lineHeight: 18, fontWeight: '700', textAlign: 'center' }}>{value.name}</Text>
                            </Pressable>
                        ))}
                    </View>
                </View>
            </SafeAreaView>

        </Modal>
    );
};

export default React.memo(HeaderModal);